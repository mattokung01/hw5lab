const express = require('express');
const router = express.Router();
const s6123Controller = require('../controllers/loginController');
const loginValidator = require('../controllers/loginsValidator');

router.get('/login',s6123Controller.list);
router.post('/login/add',loginValidator.addValidator,s6123Controller.add);
router.get('/login/deleteNow/:id',s6123Controller.deleteNow);
router.get('/login/delete/:id',s6123Controller.delete);

router.get('/login/update/:id',s6123Controller.edit);
router.post('/login/update/:id',loginValidator.updateValidator,s6123Controller.update);
router.get('/login/new',s6123Controller.new);

module.exports = router;  