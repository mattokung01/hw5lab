const express = require('express');
const router = express.Router();

const ce6103Controller = require('../controllers/ce611998003Controller');
const ce6103Validator = require('../controllers/ce611998003Validator');

router.get('/ce611998003',ce6103Controller.list);

router.get('/ce611998003/add',ce6103Controller.add);
router.post('/ce611998003/save',ce6103Validator.addValidator,ce6103Controller.save);

router.get('/ce611998003/del/:id',ce6103Controller.del);
router.get('/ce611998003/delete/:id',ce6103Controller.delete);

router.get('/ce611998003/edit/:id',ce6103Controller.edit);
router.post('/ce611998003/update/:id',ce6103Validator.updateValidator,ce6103Controller.update);

module.exports = router;
