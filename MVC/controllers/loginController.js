const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
    // if( typeof req.session.user == 'underfined') { 
    //     res.redirect('/');
    // }else{
    //     res.render('login/loginList',{session: req.session});
    // }
    if(req.session.user){
    req.getConnection((err,conn) =>{
        conn.query('SELECT * FROM user',(err,s6123e) =>{
            if(err){
                res.json(err);
            }
            res.render('login/loginList',{session: req.session,data:s6123e});
        });
    });
    }else {
    res.redirect('/');
    }
};

controller.add = (req,res) => {
    if(req.session.user){
        if(req.session.moduleF3k4==0){res.redirect("/home");}
        const data=req.body;
    console.log(data);
    const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/login/new');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
                conn.query('INSERT INTO user set ?',[data],(err,s6123e)=>{
            res.redirect('/login');
                });
            });
        }
    }else {
    res.redirect('/');
    }
};
controller.delete = (req,res) => {
    if(req.session.user){
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM user',[id],(err,s6123e)=>{/////Update
            if(err){
                res.json(err);
            }
            res.render('login/loginDelete',{session: req.session,data:s6123e[0]});
        });
    });    }else {
        res.redirect('/');
        }
};

controller.deleteNow = (req,res) => {
    if(req.session.user){
    const { id } = req.params;
    const  errorss= {errors: [{ value: '', msg: 'ลบข้อมูลนี้ไม่ได้ ', param: '', location: '' }]}
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM user WHERE id= ?',[id],(err,s6123e)=>{
            if(err){
                req.session.errors=errorss;
                req.session.success=false;
            }else{
            console.log(s6123e);
            req.session.success=true;
            req.session.topic="ลบข้อมูลเสร็จแล้ว";
            }
            res.redirect('/login');
        });
    }); }else {
        res.redirect('/');
        }
};

controller.edit = (req,res) => {
    if(req.session.user){
        if(req.session.moduleF3k4==0){res.redirect("/home");}
    const { id } = req.params;
        req.getConnection((err,conn)=>{
            conn.query('SELECT * FROM user WHERE id= ?',[id],(err,s6123e)=>{
                if(err){
                    res.json(err);
                }
                res.render('login/loginUpdate',{session: req.session,data:s6123e[0]});
            });
        });
    }else {
        res.redirect('/');
        }
    };

controller.update = (req,res) => {
    if(req.session.user){
        if(req.session.moduleF3k4==0){res.redirect("/home");}
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
    console.log(data);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM user WHERE id= ?',[id],(err,s6123e)=>{
                        res.render('login/loginUpdate',{
                        session: req.session,
                        data:s6123e[0]});
                });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE user SET ?  WHERE id = ?',[data,id],(err,s6123e) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/login');
               });
             });
        }}else {
            res.redirect('/');
            }
}

controller.new = (req,res) => {
    if(req.session.user){
        if(req.session.moduleF3k4==0){res.redirect("/home");}
    const data = null;
    res.render('login/loginAdd',{session: req.session,data:data});
    }else {
    res.redirect('/');
    }
};


module.exports = controller;
