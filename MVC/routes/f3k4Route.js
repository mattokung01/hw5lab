const express = require('express');
const router = express.Router();

const homeController = require('../controllers/f3k4Controller');
const validator = require('../controllers/f3k4Validator');

router.get('/', homeController.log);
router.post('/login', validator.loginValidator, homeController.inLogin);
router.get('/home', homeController.loghome);
router.get('/logout', homeController.logout);


module.exports = router;