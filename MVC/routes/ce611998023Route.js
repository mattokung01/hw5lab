const express = require('express');
const router = express.Router();

const ce6123Controller = require('../controllers/ce611998023Controller');
const ce6123Validator = require('../controllers/ce611998023Validator');

router.get('/ce611998023',ce6123Controller.list);

router.get('/ce611998023/add',ce6123Controller.add);
router.post('/ce611998023/save',ce6123Validator.addValidator,ce6123Controller.save);

router.get('/ce611998023/del/:id',ce6123Controller.del);
router.get('/ce611998023/delete/:id',ce6123Controller.delete);

router.get('/ce611998023/edit/:id',ce6123Controller.edit);
router.post('/ce611998023/update/:id',ce6123Validator.updateValidator,ce6123Controller.update);

module.exports = router;  