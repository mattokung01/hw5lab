const controller = {};
const { validationResult } = require('express-validator');
const { default: validator } = require('validator');

controller.log = (req, res) => {
    res.render('login', { session: req.session });
};

controller.loghome = function(req, res) {
    if (req.session.user) {
        res.render('index', { session: req.session });
    } else {
        res.render('login', { session: req.session });
    }
};

controller.inLogin = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;
        res.redirect('/');
    } else {
            const username=req.body.username;
            const password=req.body.password;
            req.getConnection((err,conn) =>{
                conn.query('Select * from user where username= ? AND password= ?',[username,password],(err,data) => {
                    if(err){
                        res.json(err);
                    }else{
                        if (data.length>0) {
                            req.session.user = data[0].id;
                                conn.query('SELECT * FROM permission WHERE module_id=1 AND user_id= ?',[req.session.user],(err2,moduleCe611998023) => {
                                    conn.query('SELECT * FROM permission WHERE module_id=2 AND user_id= ?',[req.session.user],(err2,moduleCe611998006) => {
                                        conn.query('SELECT * FROM permission WHERE module_id=3 AND user_id= ?',[req.session.user],(err2,moduleCe611998012) => {
                                            conn.query('SELECT * FROM permission WHERE module_id=4 AND user_id= ?',[req.session.user],(err2,moduleCe611998003) => {
                                                conn.query('SELECT * FROM permission WHERE module_id=5 AND user_id= ?',[req.session.user],(err2,moduleCe6119980xx) => {
                                                    if(moduleCe611998023.length>0){
                                                        req.session.moduleCe611998023=1;//มีสิทธิ์    
                                                    }else{
                                                        req.session.moduleCe611998023=0;//ไม่มีสิทธิ์
                                                    }
                                                    if(moduleCe611998006.length>0){
                                                        req.session.moduleCe611998006=1;//มีสิทธิ์    
                                                    }else{
                                                        req.session.moduleCe611998006=0;
                                                    }
                                                    if(moduleCe611998012.length>0){
                                                        req.session.moduleCe611998012=1;//มีสิทธิ์    
                                                    }else{
                                                        req.session.moduleCe611998012=0;
                                                    }
                                                    if(moduleCe611998003.length>0){
                                                        req.session.moduleCe611998003=1;//มีสิทธิ์    
                                                    }else{
                                                        req.session.moduleCe611998003=0;
                                                    }
                                                    if(moduleCe6119980xx.length>0){
                                                        req.session.moduleCe6119980xx=1;
                                                        req.session.moduleCe611998023=1;//มีสิทธิ์    
                                                        req.session.moduleCe611998006=1;
                                                        req.session.moduleCe611998012=1;
                                                        req.session.moduleCe611998003=1;
                                                    }else{
                                                        req.session.moduleCe6119980xx=0;//ไม่มีสิทธิ์
                                                    }
                                                    req.session.topic = "ผู้ใช้งานได้เข้าสู่ระบบ Login สำเร็จ";
                                                    res.redirect('/home');
                                                });
                                            });
                                        });
                                    });
                                });
                    } else {
                res.redirect('/');
            }
        }
    });
});
    };
};

controller.logout = function(req, res) {
    req.session.destroy();
    res.redirect('/');
};


module.exports = controller;
