const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
    if(req.session.user){
    req.getConnection((err,conn) =>{
        conn.query('SELECT oat.id as oatId, scit611998003, mirot611998003, id_611998023, oat.id_611998006 id_611998006s, scit611998023, scit611998006 FROM ce611998003 as oat left join ce611998023 as boy on oat.id_611998023 = boy.id left join ce611998006 as o on oat.id_611998006 = o.id',(err,s6123e) =>{
            if(err){
                res.json(err);
            }
            res.render('ce611998003/ce611998003List',{session: req.session,data:s6123e});
        });
    });}else {
        res.redirect('/');
        }
};

controller.save = (req,res) => {
    if(req.session.user && req.session.moduleCe611998003==1){
    const data=req.body;
    if(data.id_611998023==""){data.id_611998023 = null;}
    if(data.id_611998006==""){data.id_611998006 = null;}
    // console.log(data.id_chanchai);
    // console.log(data);
    const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/ce611998003/add');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
                conn.query('INSERT INTO ce611998003 set ?',[data],(err,s6123e)=>{
                    res.redirect('/ce611998003');
                });
            });
        }}else {
            res.redirect('/home');
            }
};

controller.del = (req,res) => {
    if(req.session.user && req.session.moduleCe611998003==1){
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('SELECT oat.id oatId, scit611998003, mirot611998003, id_611998023, oat.id_611998006 id_611998006s, scit611998023, scit611998006 FROM ce611998003 as oat left join ce611998023 as boy on oat.id_611998023 = boy.id left join ce611998006 as o on oat.id_611998006 = o.id HAVING oat.id= ?',[id],(err,s6123e)=>{/////Update
            if(err){
                res.json(err);
            }
            res.render('ce611998003/ce611998003Delete',{session: req.session,data:s6123e[0]});
        });
    });}else {
        res.redirect('/home');
        }
};

controller.delete = (req,res) => {
    if(req.session.user && req.session.moduleCe611998003==1){
    const { id } = req.params;
    const  errorss= {errors: [{ value: '', msg: 'ลบข้อมูลนี้ไม่ได้ ', param: '', location: '' }]}/////Update
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM ce611998003 WHERE id= ?',[id],(err,s6123e)=>{
            if(err){
                req.session.errors=errorss;/////Update
                req.session.success=false;/////Update
            }else{
            console.log(s6123e);
            req.session.success=true;
            req.session.topic="ลบข้อมูลเสร็จแล้ว";

          }
          res.redirect('/ce611998003')
        });
    });}else {
        res.redirect('/home');
        }
};

controller.edit = (req,res) => {
    if(req.session.user && req.session.moduleCe611998003==1){
    const { id } = req.params;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM ce611998003 WHERE id= ?',[id],(err,s6123e)=>{
                    conn.query('SELECT * FROM ce611998023',(err,s6106e)=>{
                        conn.query('SELECT * FROM ce611998006',(err,s6112e)=>{
                            res.render('ce611998003/ce611998003Update',{
                                session: req.session,
                                data1:s6123e[0],
                                data2:s6106e,
                                data3:s6112e});
                        });
                    });
                });
            });}else {
                res.redirect('/home');
                }
        }

controller.update = (req,res) => {
    if(req.session.user && req.session.moduleCe611998003==1){
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
    if(data.id_611998023==""){data.id_611998023 = null;}
    if(data.id_611998006==""){data.id_611998006 = null;}
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM ce611998003 WHERE id= ?',[id],(err,s6123e)=>{
                    conn.query('SELECT * FROM ce611998023',(err,s6106e)=>{
                        conn.query('SELECT * FROM ce611998006',(err,s6112e)=>{
                            res.render('ce611998003/ce611998003Update',{
                                session: req.session,
                                data1:s6123e[0],
                                data2:s6106e,
                                data3:s6112e});
                        });
                    });
                });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE  ce611998003 SET ?  WHERE id = ?',[data,id],(err,s6123e) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/ce611998003');
               });
             });
        }}else {
            res.redirect('/home');
            }
}

controller.add = (req,res) => {
    if(req.session.user && req.session.moduleCe611998003==1){
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM ce611998023',(err,s6106e) =>{
        conn.query('SELECT * FROM ce611998006',(err,s6112e) =>{
            res.render('ce611998003/ce611998003Add',{data1:s6106e,data2:s6112e,session: req.session});
        });
      });
    });}else {
        res.redirect('/home');
        }
  };

module.exports = controller;
