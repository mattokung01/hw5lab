const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
    if(req.session.user){
    req.getConnection((err,conn) =>{
        conn.query('SELECT boy.id boyId, scit611998023, mirot611998023, id_611998006, boy.id_611998012 id_611998012s, scit611998006, scit611998012 FROM ce611998023 boy left join ce611998006 o on boy.id_611998006 = o.id left join ce611998012 arm on boy.id_611998012 = arm.id',(err,s6123e) =>{
            if(err){
                res.json(err);
            }
            res.render('ce611998023/ce611998023List',{session: req.session,data:s6123e});
        });
    });}else {
        res.redirect('/');
        }
};

controller.save = (req,res) => {
    if(req.session.user && req.session.moduleCe611998023==1){
    const data=req.body;
    if(data.id_611998006==""){data.id_611998006 = null;}
    if(data.id_611998012==""){data.id_611998012 = null;}
    // console.log(data.id_chanchai);
    // console.log(data);
    const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/ce611998023/add');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
                conn.query('INSERT INTO ce611998023 set ?',[data],(err,s6123e)=>{
                    res.redirect('/ce611998023');
                });
            });
        }}else {
            res.redirect('/home');
            }
};

controller.del = (req,res) => {
    if(req.session.user && req.session.moduleCe611998023==1){
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('SELECT boy.id boyId, scit611998023, mirot611998023, id_611998006, boy.id_611998012 id_611998012s, scit611998006, scit611998012 FROM ce611998023 boy left join ce611998006 o on boy.id_611998006 = o.id left join ce611998012 arm on boy.id_611998012 = arm.id HAVING boy.id= ?',[id],(err,s6123e)=>{/////Update
            if(err){
                res.json(err);
            }
            res.render('ce611998023/ce611998023Delete',{session: req.session,data:s6123e[0]});
        });
    });}else {
        res.redirect('/home');
        }
};

controller.delete = (req,res) => {
    if(req.session.user && req.session.moduleCe611998023==1){
    const { id } = req.params;
    const  errorss= {errors: [{ value: '', msg: 'ลบข้อมูลนี้ไม่ได้ ', param: '', location: '' }]}/////Update
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM ce611998023 WHERE id= ?',[id],(err,s6123e)=>{
            if(err){
                req.session.errors=errorss;/////Update
                req.session.success=false;/////Update
            }else{
            console.log(s6123e);
            req.session.success=true;
            req.session.topic="ลบข้อมูลเสร็จแล้ว";
            }
            res.redirect('/ce611998023');
        });
    });}else {
        res.redirect('/home');
        }
};

controller.edit = (req,res) => {
    if(req.session.user && req.session.moduleCe611998023==1){
    const { id } = req.params;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM ce611998023 WHERE id= ?',[id],(err,s6123e)=>{
                    conn.query('SELECT * FROM ce611998006',(err,s6106e)=>{
                        conn.query('SELECT * FROM ce611998012',(err,s6112e)=>{
                            res.render('ce611998023/ce611998023Update',{
                                session: req.session,
                                data1:s6123e[0],
                                data2:s6106e,
                                data3:s6112e});
                        });
                    });
                });
            });}else {
                res.redirect('/home');
                }
        }

controller.update = (req,res) => {
    if(req.session.user && req.session.moduleCe611998023==1){
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
    if(data.id_611998006==""){data.id_611998006 = null;}
    if(data.id_611998012==""){data.id_611998012 = null;}
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM ce611998023 WHERE id= ?',[id],(err,s6123e)=>{
                    conn.query('SELECT * FROM ce611998006',(err,s6106e)=>{
                        conn.query('SELECT * FROM ce611998012',(err,s6112e)=>{
                            res.render('ce611998023/ce611998023Update',{
                                session: req.session,
                                data1:s6123e[0],
                                data2:s6106e,
                                data3:s6112e});
                        });
                    });
                });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE  ce611998023 SET ?  WHERE id = ?',[data,id],(err,s6123e) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/ce611998023');
               });
             });
        }}else {
            res.redirect('/home');
            }
    }

controller.add = (req,res) => {
    if(req.session.user && req.session.moduleCe611998023==1){
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM ce611998006',(err,s6106e) =>{
        conn.query('SELECT * FROM ce611998012',(err,s6112e) =>{
            res.render('ce611998023/ce611998023Add',{data1:s6106e,data2:s6112e,session: req.session});
        });
      });
    });}else {
    res.redirect('/home');
    }
  };

module.exports = controller;
