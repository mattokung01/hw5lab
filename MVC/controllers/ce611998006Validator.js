const { check } = require('express-validator');

exports.addValidator = [
    check('scit611998006',"Tab 1 ไม่ถูกต้อง").not().isEmpty(),
    check('mirot611998006',"Tab 2 ไม่ถูกต้อง").isFloat()];

exports.updateValidator = [
    check('scit611998006',"Tab 1 ไม่ถูกต้อง").not().isEmpty(),
    check('mirot611998006',"Tab 2 ไม่ถูกต้อง").isFloat()];
