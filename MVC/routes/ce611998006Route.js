const express = require('express');
const router = express.Router();

const ce6106Controller = require('../controllers/ce611998006Controller');
const ce6106Validator = require('../controllers/ce611998006Validator');

router.get('/ce611998006',ce6106Controller.list);
router.get('/ce611998006/add',ce6106Controller.add);
router.post('/ce611998006/save',ce6106Validator.addValidator,ce6106Controller.save);
router.get('/ce611998006/del/:id',ce6106Controller.del);
router.get('/ce611998006/delete/:id',ce6106Controller.delete);
router.get('/ce611998006/edit/:id',ce6106Controller.edit);
router.post('/ce611998006/update/:id',ce6106Validator.updateValidator,ce6106Controller.update);

module.exports = router;
