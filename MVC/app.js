const express=require('express');
const body =require('body-parser');
const cookie= require('cookie-parser');
const session= require('express-session');
const mysql=require('mysql');
const connection=require('express-myconnection')
const app= express();

app.use(express.static('public'));
app.set('view engine','ejs');
app.set('views','views');

app.use(body.urlencoded({extended: true}));
app.use(cookie());
app.use(session({secret:'Passw0rd',
resave: true,
saveUninitialized: true
}));
app.use(connection(mysql,{
  host:'localhost',
  user:'root',
  password:'Passw0rd',
  port:3306,
  database:'f3k4'
},'single'));

const s6106Route = require('./routes/ce611998006Route');
app.use('/',s6106Route);

const s6103Route =require('./routes/ce611998003Route');
app.use('/',s6103Route);

const s6123Route =require('./routes/ce611998023Route');
app.use('/',s6123Route);

const s6112Route = require('./routes/ce611998012Route');
app.use('/',s6112Route);

const loginRoute = require('./routes/f3k4Route');
app.use('/', loginRoute);

const logine = require('./routes/loginRoute');
app.use('/', logine);

 app.listen(8081,'127.0.0.1');
