const express = require('express');
const router = express.Router();

const ce6112Controller = require('../controllers/ce611998012Controller');
const ce6112Validator = require('../controllers/ce611998012Validator');

router.get('/ce611998012',ce6112Controller.list);

router.get('/ce611998012/add',ce6112Controller.add);
router.post('/ce611998012/save',ce6112Validator.addValidator,ce6112Controller.save);

router.get('/ce611998012/del/:id',ce6112Controller.del);
router.get('/ce611998012/delete/:id',ce6112Controller.delete);

router.get('/ce611998012/edit/:id',ce6112Controller.edit);
router.post('/ce611998012/update/:id',ce6112Validator.updateValidator,ce6112Controller.update);

module.exports = router;
