const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
    if(req.session.user){
    req.getConnection((err,conn) =>{
        conn.query('SELECT arm.id as armid, scit611998012, mirot611998012, id_611998003, arm.id_611998023 id_611998023s, scit611998003, scit611998023 FROM ce611998012 arm left join ce611998003 o on arm.id_611998003 = o.id left join ce611998023 boy on arm.id_611998023 = boy.id',(err,s6112e) =>{
            if(err){
                res.json(err);
            }
            res.render('ce611998012/ce611998012List',{session: req.session,data:s6112e});
        });
    });}else {
        res.redirect('/');
        }
};
//*********************************************************************************************************************************
controller.save = (req,res) => {
    if(req.session.user && req.session.moduleCe611998012==1){
    const data=req.body;
    if(data.id_611998003==""){data.id_611998003 = null;}
    if(data.id_611998023==""){data.id_611998023 = null;}
    // console.log(data.id_chanchai);
    // console.log(data);
    const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/ce611998012/add');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
                conn.query('INSERT INTO ce611998012 set ?',[data],(err,s6112e)=>{
                    res.redirect('/ce611998012');
                });
            });
        }}else {
            res.redirect('/home');
            }
};
//***********************************************************************************************************************************************
controller.del = (req,res) => {
    if(req.session.user && req.session.moduleCe611998012==1){
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('SELECT arm.id armid, scit611998012, mirot611998012, id_611998003, arm.id_611998023 id_611998023s, scit611998003, scit611998023 FROM ce611998012 arm left join ce611998003 o on arm.id_611998003 = o.id left join ce611998023 boy on arm.id_611998023 = boy.id HAVING arm.id= ?',[id],(err,s6112e)=>{/////Update
            if(err){
                res.json(err);
            }
            res.render('ce611998012/ce611998012Delete',{session: req.session,data:s6112e[0]});
        });
    });}else {
        res.redirect('/home');
        }
};
//*************************************************************************************************************************************
controller.delete = (req,res) => {
    if(req.session.user && req.session.moduleCe611998012==1){
    const { id } = req.params;
    const  errorss= {errors: [{ value: '', msg: 'ไม่สามารถลบข้อมูลนี้ได้ ', param: '', location: '' }]}/////Update
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM ce611998012 WHERE id= ?',[id],(err,s6112e)=>{
            if(err){
                req.session.errors=errorss;/////Update
                req.session.success=false;/////Update
            }else{
            console.log(s6112e);
            req.session.success=true;
            req.session.topic="ลบข้อมูลเสร็จแล้ว";

          }
          res.redirect('/ce611998012')

        });
    });}else {
        res.redirect('/home');
        }
};
//***********************************************************************************************************************************
controller.edit = (req,res) => {
    if(req.session.user && req.session.moduleCe611998012==1){
    const { id } = req.params;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM ce611998012 WHERE id= ?',[id],(err,s6112e)=>{
                    conn.query('SELECT * FROM ce611998003',(err,s6103e)=>{
                        conn.query('SELECT * FROM ce611998023',(err,s6123e)=>{
                            res.render('ce611998012/ce611998012Update',{
                                session: req.session,
                                data1:s6112e[0],
                                data2:s6103e,
                                data3:s6123e});
                        });
                    });
                });
            });}else {
                res.redirect('/home');
                }
        }
//************************************************************************************************************************************
controller.update = (req,res) => {
    if(req.session.user && req.session.moduleCe611998012==1){
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
    if(data.id_611998003==""){data.id_611998003 = null;}
    if(data.id_611998023==""){data.id_611998023 = null;}
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM ce611998012 WHERE id= ?',[id],(err,s6112e)=>{
                    conn.query('SELECT * FROM ce611998003',(err,s6103e)=>{
                        conn.query('SELECT * FROM ce611998023',(err,s6123e)=>{
                            res.render('ce611998012/ce611998012Update',{
                                session: req.session,
                                data1:s6112e[0],
                                data2:s6103e,
                                data3:s6123e});
                        });
                    });
                });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE  ce611998012 SET ?  WHERE id = ?',[data,id],(err,s6112e) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/ce611998012');
               });
             });
        }}else {
            res.redirect('/home');
            }
}
//*********************************************************************************************************
controller.add = (req,res) => {
    if(req.session.user && req.session.moduleCe611998012==1){
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM ce611998003',(err,s6103e) =>{
        conn.query('SELECT * FROM ce611998023',(err,s6123e) =>{
            res.render('ce611998012/ce611998012Add',{data1:s6103e,data2:s6123e,session: req.session});
        });
      });
    });}else {
        res.redirect('/home');
        }
  };

module.exports = controller;
