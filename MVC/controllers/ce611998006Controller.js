const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
    if(req.session.user){
    req.getConnection((err,conn) =>{
        conn.query('SELECT o.id as oid, scit611998006, mirot611998006, id_611998012, o.id_611998003 as id6103, scit611998003, scit611998012 FROM ce611998006 as o LEFT JOIN ce611998012 as arm ON o.id_611998012 = arm.id LEFT JOIN ce611998003 as oat ON o.id_611998003 = oat.id',(err,s6106) =>{
            if(err){
                res.json(err);
            }
            res.render('ce611998006/ce611998006List',{session: req.session,data:s6106});
        });
    });}else {
        res.redirect('/');
        }
};

controller.save = (req,res) => {
    if(req.session.user && req.session.moduleCe611998006==1){
    const data=req.body;
    if(data.id_611998012==""){data.id_611998012 = null;}
    if(data.id_611998003==""){data.id_611998003 = null;}
    const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/ce611998006/add');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
                conn.query('INSERT INTO ce611998006 set ?',[data],(err,s6106)=>{
                    res.redirect('/ce611998006');
                });
            });
        }
      }else {
            res.redirect('/home');
            }
};

controller.del = (req,res) => {
    if(req.session.user && req.session.moduleCe611998006==1){
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('SELECT o.id as oid, scit611998006, mirot611998006, id_611998012, o.id_611998003 as id_6103, scit611998003, scit611998012 FROM ce611998006 as o LEFT JOIN ce611998012 as arm ON o.id_611998012 = arm.id LEFT JOIN ce611998003 as oat ON o.id_611998003 = oat.id HAVING o.id= ?',[id],(err,s6106)=>{/////Update
            if(err){
                res.json(err);
            }
            res.render('ce611998006/ce611998006Delete',{session: req.session,data:s6106[0]});
        });
    });}else {
        res.redirect('/home');
        }
};

controller.delete = (req,res) => {
    if(req.session.user && req.session.moduleCe611998006==1){
    const { id } = req.params;
    const  errorss= {errors: [{ value: '', msg: 'ลบข้อมูลนี้ไม่ได้ ', param: '', location: '' }]}/////Update
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM ce611998006 WHERE id= ?',[id],(err,s6106)=>{
            if(err){
                req.session.errors=errorss;
                req.session.success=false;
            }else{
            console.log(s6106);
            req.session.success=true;
            req.session.topic="ลบข้อมูลเสร็จแล้ว";

            }
            res.redirect('/ce611998006');
        });
    });
  }else {
        res.redirect('/home');
        }

};

controller.edit = (req,res) => {
    if(req.session.user && req.session.moduleCe611998006==1){
    const { id } = req.params;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM ce611998006 WHERE id= ?',[id],(err,s6106)=>{
                    conn.query('SELECT * FROM ce611998012',(err,s6112)=>{
                        conn.query('SELECT * FROM ce611998003',(err,s6103)=>{
                            res.render('ce611998006/ce611998006Update',{
                                session: req.session,data1:s6106[0],data2:s6112,data3:s6103});
                        });
                    });
                });
            });}else {
                res.redirect('/home');
                }

        }

controller.update = (req,res) => {
    if(req.session.user && req.session.moduleCe611998006==1){
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
    if(data.id_611998012==""){data.id_611998012 = null;}
    if(data.id_611998003==""){data.id_611998003 = null;}
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM ce611998006 WHERE id= ?',[id],(err,s6106)=>{
                    conn.query('SELECT * FROM ce611998012',(err,s6112)=>{
                        conn.query('SELECT * FROM ce611998003',(err,s6103)=>{
                            res.render('ce611998006/ce611998006Update',{
                                session: req.session,data1:s6106[0],data2:s6112,data3:s6103});
                        });
                    });
                });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE  ce611998006 SET ?  WHERE id = ?',[data,id],(err,s6106) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/ce611998006');
               });
             });
        }}else {
            res.redirect('/home');
            }
}

controller.add = (req,res) => {
    if(req.session.user && req.session.moduleCe611998006==1){
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM ce611998012',(err,s6112) =>{
        conn.query('SELECT * FROM ce611998003',(err,s6103) =>{
            res.render('ce611998006/ce611998006Add',{data1:s6112,data2:s6103,session: req.session});
        });
      });
    });}else {
        res.redirect('/home');
        }
  };

module.exports = controller;
