const { check } = require('express-validator');
exports.loginValidator = [
    check('username', "Username is Incorrect").not().isEmpty(),
    check('password', "Password is Incorrect").not().isEmpty()];